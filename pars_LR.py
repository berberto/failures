import numpy as np
import sys
import os
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument('--run', dest='run', action='store_true', default=False, help='Run code serially')
args = parser.parse_args()
run = args.run

s_vals = ["sqrt", "const"] # ["sqrt", "const", "lin", "const+", "lin+", "sqrt,lin", "sqrt,lin+", "sqrt,const", "sqrt,const+"] # initial parameters scaling

a_vals = ["linear"] #, "relu"] # initial parameters scaling

N_vals = [128, 512, 2048] # number of units per hidden layer

l_vals = [2] # [2,3,5,10] # number of layers

i_vals = [1, 1/4, 1/16] # input dimension as a fraction of the input layer size

o_vals = [1] # output dimension

p_vals = [0.5] # weight failure probability

f_vals = ["all","2"]

wd_vals = [0., 1e-4, 1e-2]

with open("pars_LR.txt", "w") as file:
    for a in a_vals:
        for s in s_vals:
            for N in N_vals:
                for l in l_vals:
                    for i in i_vals:
                        for o in o_vals:
                            for p in p_vals:
                                if p == 0.:
                                    # if NOT dropout, choose weight-decay parameters
                                    _vals = wd_vals
                                else:
                                    # if dropout, chose layers to apply it to
                                    _vals = f_vals
                                for f in _vals:
                                    _pars = f"{a}  {s}  {N}  {l}  {int(i*N)}  {o}  {p:.2f}  {f}"
                                    file.write(_pars+"\n")
                                    if run:
                                        print(80*"=")
                                        os.system("python -u failures_LR.py "+_pars)
