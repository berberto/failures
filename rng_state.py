import torch
import numpy as np

restart = True


if restart:
	rng_state = torch.load("rng_state_restart.pt")
	torch.set_rng_state(rng_state)
	xs = list(np.load("xs_restart.npy"))
	start = len(xs)
else:
	torch.manual_seed(1871)
	xs = []
	start = 0


for i in range(start, 10):
	print(i)
	x = torch.randn(5)

	xs.append( x.detach().cpu().numpy() )
	np.save("xs_restart.npy", xs)
	rng_state = torch.get_rng_state()
	torch.save(rng_state, "rng_state_restart.pt")
	if i == 4:
		exit()
